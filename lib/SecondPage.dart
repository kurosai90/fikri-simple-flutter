import 'package:flutter/material.dart';

// ignore: must_be_immutable
class SecondPage extends StatelessWidget {
  String str = "";

  SecondPage({Key key, this.str}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(str),
      ),
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ElevatedButton(
            child: Text("Option 1"),
            onPressed: () {
              Navigator.pop(context, "You selected Option 1");
            },
          ),
          ElevatedButton(
            child: Text("Option 2"),
            onPressed: () {
              Navigator.pop(context, "You selected Option 2");
            },
          ),
        ],
      )),
    );
  }
}
