import 'package:flutter/material.dart';
import 'SecondPage.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Fikri Simple Flutter'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String dataBack = "";

  gotoSecondPage(BuildContext context, String strBook) async {
    final result = await Navigator.push(context,
        MaterialPageRoute(builder: (context) => SecondPage(str: strBook)));
    if (result != null) {
      setState(() {
        dataBack = result;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
        title: Text(widget.title),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              child: Text("Comic Book"),
              onPressed: () {
                gotoSecondPage(context, "Comic Book");
              },
            ),
            ElevatedButton(
              child: Text("Novel"),
              onPressed: () {
                gotoSecondPage(context, "Novel");
              },
            ),
            Text("Data Back: $dataBack")
          ],
        ),
      ),
    );
  }
}
